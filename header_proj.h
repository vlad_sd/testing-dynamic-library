#ifndef _HEADER_PROJ_H
#define _HEADER_PROJ_H

#include <stdio.h>
#include <stdlib.h>
#include "stdbool.h"

__declspec(dllimport) bool createConfFile(char *filename);
__declspec(dllimport) FILE  *openConfFile(char *filename);
__declspec(dllimport) char*  readValueParameter(FILE  *confFile, const char* section, const char* nameParameter);
__declspec(dllimport) bool  writeValueParameter(FILE  *confFile, const char* section, const char* nameParameter, const char* value);
__declspec(dllimport) bool removeParameter(FILE  *confFile, const char* section, const char* nameParameter);
__declspec(dllimport) bool removeSection(FILE  *confFile, const char* section);
__declspec(dllimport) bool  closeFile(FILE *confFile);


#endif