
 #ifndef _STDBOOL_H_
 #define _STDBOOL_H_     
 
#define false   0
#define true    1

 #define bool    _Bool
 #if __STDC_VERSION__ < 199901L && __GNUC__ < 3
typedef int     _Bool;
#endif

 #endif /* !_STDBOOL_H_ */

 